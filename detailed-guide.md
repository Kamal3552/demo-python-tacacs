# Setup and Usage Guide 
This guide is provided as a more detailed and complete walkthrough of setting up and using the demo application than the quick-start in the README.

- [Running the Demonstration Application](#running-the-demonstration-application)
- [Building and Running the `tac_plus` server](#building-and-running-the--tac-plus--server)
- [Setting up Python Virtual Environment to Run Demo Application](#setting-up-python-virtual-environment-to-run-demo-application)
- [Starting the Flask Application](#starting-the-flask-application)
- [Exploring the Application and AAA](#exploring-the-application-and-aaa)
  * [RBAC / AAA Details for the Application](#rbac---aaa-details-for-the-application)
  * [Logging in as `customer`](#logging-in-as--customer-)
  * [Investigating Service Requests as an Operator](#investigating-service-requests-as-an-operator)
  * [Managing Requests as the `sysadmin`](#managing-requests-as-the--sysadmin-)
  * [Verifying Updated Request as `customer`](#verifying-updated-request-as--customer-)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Running the Demonstration Application
If you'd like to run this application on your own system, you'll need: 

* Docker: Used to run the [`tac_plus`](tac_plus/) server the application authenticates to
* Python 3.9: The application itself was written in Python 3.9.4.  New versions of Python should be fine.  Older versions may have issues

## Building and Running the `tac_plus` server
In order for an application to use TACACS for AAA/RBAC there must be a TACACS server to receive the messages and reply.  While any TACACS Server should work, this demonstration uses the `tac_plus` server for Linux and includes a Dockerfile and Makefile to make building and running the server easy. 

The [`Dockerfile`](tac_plus/Dockerfile) will: 

* Download the source for `tac_plus` and build it in a `build` container
* Install the built `tac_plus` binary and files into a new container image
* Install all requirements to run `tac_plus` in the container 
* Add an entrypoint script to startup `tac_plus` when a container starts 
* Add a default `tac_plus.conf` file to the container configured for use with the demo app 
    * The `tac_plus.conf` file can be overriden by bind mounting a replacement file when running the container

1. Navigate to the `tac_plus/` directory to build and start the server.

    ```
    cd tac_plus
    ls -l 

    total 16
    -rw-r--r--  1 hpreston  staff   1.3K Mar 23 19:34 Dockerfile
    -rw-r--r--  1 hpreston  staff   677B Mar 23 19:33 Makefile
    drwxr-xr-x  5 hpreston  staff   160B Mar 13 13:01 files
    drwxr-xr-x  4 hpreston  staff   128B May  3 12:43 logs
    ```

1. Build the image using the Makefile

    ```
    make build
    docker build \
            --build-arg DEPLOY_TYPE=dev \
            -t tac_plus:latest \
            .
    [+] Building 0.5s (16/16) FINISHED                                                                                                                                                   
    => [internal] load build definition from Dockerfile                                                                                                                            0.0s
    => => transferring dockerfile: 37B                                                                                                                                             0.0s
    => [internal] load .dockerignore                                                                                                                                               0.0s
    => => transferring context: 2B                                                                                                                                                 0.0s
    => [internal] load metadata for docker.io/library/ubuntu:22.04                                                                                                                 0.0s
    => https://shrubbery.net/pub/tac_plus/tacacs-F4.0.4.28.tar.gz                                                                                                                  0.4s
    => [internal] load build context                                                                                                                                               0.1s
    => => transferring context: 116B                                                                                                                                               0.0s
    => [build 1/5] FROM docker.io/library/ubuntu:22.04                                                                                                                             0.0s
    => CACHED [stage-1 2/6] RUN mkdir /var/log/tac_plus                                                                                                                            0.0s
    => CACHED [stage-1 3/6] RUN apt-get update && apt-get install -y   libwrap0-dev   && apt clean                                                                                 0.0s
    => CACHED [build 2/5] RUN apt-get update && apt-get install -y   wget   make   gcc   flex   bison   libwrap0-dev                                                               0.0s
    => CACHED [build 3/5] ADD https://shrubbery.net/pub/tac_plus/tacacs-F4.0.4.28.tar.gz /opt                                                                                      0.0s
    => CACHED [build 4/5] RUN cd /opt && tar -xzf tacacs-F4.0.4.28.tar.gz                                                                                                          0.0s
    => CACHED [build 5/5] RUN cd /opt/tacacs-F4.0.4.28 &&     ./configure --prefix=/tacacs &&     make &&     make install                                                         0.0s
    => CACHED [stage-1 4/6] COPY --from=build /tacacs /tacacs                                                                                                                      0.0s
    => CACHED [stage-1 5/6] COPY files/dev.tac_plus.conf /etc/tacacs/tac_plus.conf                                                                                                 0.0s
    => CACHED [stage-1 6/6] COPY files/docker-entrypoint.sh /docker-entrypoint.sh                                                                                                  0.0s
    => exporting to image                                                                                                                                                          0.0s
    => => exporting layers                                                                                                                                                         0.0s
    => => writing image sha256:64bb58b7987bf2739f950714719e27bbaf983d5db7cdb9b87124ec7f50af8400                                                                                    0.0s
    => => naming to docker.io/library/tac_plus:latest  
    ```

1. Start an instance of the `tac_plus` container in the background with the Makefile. 

    ```
    make run
    docker run -it -d \
                    --name tac_plus \
                    -v /Users/hpreston/code/demo-python-tacacs/tac_plus/logs:/var/log/tac_plus \
                    -v /Users/hpreston/code/demo-python-tacacs/tac_plus/files/dev.tac_plus.conf:/etc/tacacs/tac_plus.conf \
                    -p 49:49 \
                    tac_plus:latest 
    d80b7099cb65dd7cc41c3dee9b37c57de27403976c79b2fb2c2c8ad06a24c755
    ```

1. The `tac_plus` logs will be found in the `logs/` directory.  
    * `logs/tac_plus.log` : Primary log file for the server.
    * `logs/tac_plus.acct` : Accounting log file for the server

1. Starting a `tail` on these files is a great way to watch the process of AAA. 

    ```
    tail -f logs/tac_plus.log 
    Tue May  3 16:43:53 2022 [1]: Reading config
    Tue May  3 16:43:53 2022 [1]: Version F4.0.4.28 Initialized 1
    Tue May  3 16:43:53 2022 [1]: tac_plus server F4.0.4.28 starting
    Tue May  3 16:43:53 2022 [1]: socket FD 3 AF 2
    Tue May  3 16:43:53 2022 [1]: uid=0 euid=0 gid=0 egid=0 s=-83561888
    ```

1. You can manage the status of the `tac_plus` server with the following Makefile targets. 
    * `make build` - Build a new `tac_plus` image 
    * `make run` - Start a new `tac_plus` container and run in background
    * `make stop` - Stop the running `tac_plus` container, but leave it in place 
    * `make start` - Start a stopped `tac_plus` container
    * `make delete` - Remove a stopped `tac_plus` container 
    * `make clean` - Stop and Remove the `tac_plus` container 
    * `make shell` - Start a new `tac_plus` container and attach to terminal. Do NOT start the tac_plus server, but drop into bash shell 
    * `make attach` - Attach a terminal to a running and backgrounded `tac_plus` container 

## Setting up Python Virtual Environment to Run Demo Application 
Before you can start up the `network_request` web application, you need to setup a Python virtual environment with the required libraries and install the application.  

1. Create and source a new Python 3.9 virtual environment. 

    ```
    python3.9 -m venv venv
    source venv/bin/activate
    ```

1. Install the application using `pip`

    ```
    pip install .
    Processing /Users/hpreston/code/demo-python-tacacs
    Collecting Flask
    Downloading Flask-2.1.2-py3-none-any.whl (95 kB)
        |████████████████████████████████| 95 kB 1.8 MB/s 
    Collecting tacacs-plus
    Using cached tacacs_plus-2.6-py2.py3-none-any.whl (17 kB)
    Collecting click>=8.0
    Downloading click-8.1.3-py3-none-any.whl (96 kB)
        |████████████████████████████████| 96 kB 4.7 MB/s 
    Collecting importlib-metadata>=3.6.0
    Using cached importlib_metadata-4.11.3-py3-none-any.whl (18 kB)
    Collecting itsdangerous>=2.0
    Downloading itsdangerous-2.1.2-py3-none-any.whl (15 kB)
    Collecting Jinja2>=3.0
    Downloading Jinja2-3.1.2-py3-none-any.whl (133 kB)
        |████████████████████████████████| 133 kB 3.8 MB/s 
    Collecting Werkzeug>=2.0
    Downloading Werkzeug-2.1.2-py3-none-any.whl (224 kB)
        |████████████████████████████████| 224 kB 4.4 MB/s 
    Collecting zipp>=0.5
    Using cached zipp-3.8.0-py3-none-any.whl (5.4 kB)
    Collecting MarkupSafe>=2.0
    Using cached MarkupSafe-2.1.1-cp39-cp39-macosx_10_9_universal2.whl (17 kB)
    Collecting six
    Using cached six-1.16.0-py2.py3-none-any.whl (11 kB)
    Using legacy 'setup.py install' for network-request, since package 'wheel' is not installed.
    Installing collected packages: zipp, MarkupSafe, Werkzeug, six, Jinja2, itsdangerous, importlib-metadata, click, tacacs-plus, Flask, network-request
        Running setup.py install for network-request ... done
    Successfully installed Flask-2.1.2 Jinja2-3.1.2 MarkupSafe-2.1.1 Werkzeug-2.1.2 click-8.1.3 importlib-metadata-4.11.3 itsdangerous-2.1.2 network-request-1.0.0 six-1.16.0 tacacs-plus-2.6 zipp-3.8.0
    ```

## Starting the Flask Application 
Now you can startup a "development" instance of the application on your device. 

> You will need to setup a series of environment variables that control the configuration of the application.  These are provided in the file `.envrc.dev`. 
>
> * `export TACACS_HOST=0.0.0.0` : Address for the TACACS Server.  This assumes it is running locally. If you are running the `tac_plus` container on another host, update this address. 
> * `export TACACS_SECRET=labtacacskey` : The secret key used to communication with the TACACS Server.  This must match the configuration in the `tac_plus` server. 
> * `export TACACS_APP_NAME=dev-network-request` : The name used by this application in messages to the TACACS server. It will show up in log files. 
> * `export FLASK_ENV=development` : Start the Flask app in development mode.  This turns on extra debugs in Flask, and will auto-restart the application if you change a file. 
> * `export FLASK_APP=network_request` : The name of the Flask Application, and from practical purposes the directory/library name where the code can be found when starting the application. 
> * `export LOAD_SAMPLE_DATA=True` : A flag to startup the application with a few sample network service requests to save time adding new ones. 

1. Read the ENVs into your terminal. 

    ```
    source .envrc.dev
    ```

1. Start the Flask application

    ```
    flask run

    * Serving Flask app 'network_request' (lazy loading)
    * Environment: development
    * Debug mode: on
    [03/05/2022 13:05:37.077] [INFO] [_internal.py] [_log():224]  * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)
    [03/05/2022 13:05:37.077] [INFO] [_internal.py] [_log():224]  * Restarting with stat
    * Debugger is active!
    * Debugger PIN: 100-998-540
    [03/05/2022 13:05:37.185] [INFO] [_internal.py] [_log():224]  * Debugger PIN: 100-998-540
    ```    

1. You can now navigate to the address listed in the output (ie `http://127.0.0.1:5000`). *Details on how to use the application are in the next section.*

    ![App Home Unauthenticated User](readme-images/home-unauth.png)

## Exploring the Application and AAA
Now that the application is up and running, you can now explore it and how the TACACS AAA works. 

### RBAC / AAA Details for the Application
The application has three rights/roles that users can be assigned.  

These rights and their groups are: 

* `manage` : The ability to approve or deny network service requests. Can also set the VLAN Id used for a new request.
* `read` : The ability to view **all** network service requests that have been submitted. 
* `submit` : The ability to make a request for a new service, and view the status of your own requests.

The TACACS Server has 3 groups configured that control these rights. 

* TACACS group `admin`
    * `manage` : allow
    * `read` : allow
    * `submit` : allow
* TACACS group `operator` 
    * `manage` : reject
    * `read` : allow
    * `submit` : allow
* TACACS group `submitter`
    * `manage` : reject
    * `read` : reject
    * `submit` : allow


The following are the users configured on the TACACS Server and the group they are a member of. 

|  Username  |  Password  |  TACACS Group |
| ---------  |  --------  |  ------------ |
| `sysadmin` | `password` |  `admin`      |
| `operator` | `password` |  `operator`   |
| `customer` | `password` |  `submitter`  |

Use these accounts when exploring the application. 

### Logging in as `customer`

1. Log into the application using the `customer` account. 

    ![](readme-images/login-customer.png)

1. You should now see the homepage has changed. It now offers a **Submit** button for the new request form, and a table of submitted requests.  This table is currently empty as no requests have been submitted. 

    ![](readme-images/home-customer-01.png)

1. Before we make a request, let's look at the logs from the TACACS Server. 
    > Reminder, the logs are stored in the folder `tac_plus/logs` 

1. Start by looking at the accounting log file `tac_plus.acct`.  You should see a list of messages similar to the following. 

    ```
    May  3 17:07:47 172.17.0.1      None    python_tacacs   dev-network-request     start   service=page-access     message=Application page [/] accessed by [None] from remote_addr [127.0.0.1]
    May  3 17:25:17 172.17.0.1      None    python_tacacs   dev-network-request     start   service=page-access     message=Application page [/login] accessed by [None] from remote_addr [127.0.0.1]
    May  3 17:26:35 172.17.0.1      None    python_tacacs   dev-network-request     start   service=page-access     message=Application page [/login] accessed by [None] from remote_addr [127.0.0.1]
    May  3 17:26:35 172.17.0.1      customer        python_tacacs   dev-network-request     start   service=login   message=username 'customer' attempting to login
    May  3 17:26:35 172.17.0.1      customer        python_tacacs   dev-network-request     start   service=login   message=username 'customer' logged in
    May  3 17:26:35 172.17.0.1      customer        python_tacacs   dev-network-request     start   service=authorize     message=username 'customer' attempting to authorize
    May  3 17:26:35 172.17.0.1      customer        python_tacacs   dev-network-request     start   service=page-access   message=Application page [/home] accessed by [customer] from remote_addr [127.0.0.1]
    ```

    * The first three messages record the navigating in the application done before logging in as customer. First we opened the homepage (`/`), then we navigated to the login page (`/login`) to provide our credentials. The second access to the login page was when we submitted the form and it was processed. 
    * The next two messages are the process of authenticating `customer`.  The first message indicates the attempt to authenticate is to be made, and the second that the authentication was successful. 
    * The next message is the indication that the application requested the rights of the user to *authorize* them within the application.  
    * And the final message was the redirection back to home after successfully logging in. 

1. The `tac_plus.log` file should have several messages by now as the server is configured to log all messages related to all three "A"'s.  Take a look and compare what you see to these examples.
    * Each accounting log made will include several lines indicating an accounting request.  Here is an example from the final message above. 

        ```
        Tue May  3 17:26:35 2022 [17]: Start accounting request
        Tue May  3 17:26:35 2022 [17]: 'May  3 17:26:35'
        Tue May  3 17:26:35 2022 [17]: '        '
        Tue May  3 17:26:35 2022 [17]: '172.17.0.1'
        Tue May  3 17:26:35 2022 [17]: '        '
        Tue May  3 17:26:35 2022 [17]: 'customer'
        Tue May  3 17:26:35 2022 [17]: '        '
        Tue May  3 17:26:36 2022 [17]: 'python_tacacs'
        Tue May  3 17:26:36 2022 [17]: '        '
        Tue May  3 17:26:36 2022 [17]: 'dev-network-request'
        Tue May  3 17:26:36 2022 [17]: '        '
        Tue May  3 17:26:36 2022 [17]: 'start   '
        Tue May  3 17:26:36 2022 [17]: 'service=page-access'
        Tue May  3 17:26:36 2022 [17]: '        '
        Tue May  3 17:26:36 2022 [17]: 'message=Application page [/home] accessed by [customer] from remote_addr [127.0.0.1]'
        Tue May  3 17:26:36 2022 [17]: '
        '
        ```

        * Accounting logs are better viewed in the accounting log file.
    * Logs related to the "authentication" process look like this. 

        ```
        Tue May  3 17:26:35 2022 [13]: login query for 'customer' port python_tacacs from 172.17.0.1 accepted

        # A rejected authentication is
        Tue May  3 17:41:41 2022 [22]: login query for 'customer' port python_tacacs from 172.17.0.1 rejected
        ```

    * Logs related to the "authorization" process look like this. 

        ```
        Tue May  3 17:26:35 2022 [16]: Start authorization request
        Tue May  3 17:26:35 2022 [16]: do_author: user='customer'
        Tue May  3 17:26:35 2022 [16]: user 'customer' found
        Tue May  3 17:26:35 2022 [16]: nas:service=rights (passed thru)
        Tue May  3 17:26:35 2022 [16]: nas:absent, server:read=reject -> add read=reject (k)
        Tue May  3 17:26:35 2022 [16]: nas:absent, server:submit=allow -> add submit=allow (k)
        Tue May  3 17:26:35 2022 [16]: nas:absent, server:manage=reject -> add manage=reject (k)
        Tue May  3 17:26:35 2022 [16]: added 3 args
        Tue May  3 17:26:35 2022 [16]: out_args[0] = service=rights input copy discarded
        Tue May  3 17:26:35 2022 [16]: out_args[1] = read=reject compacted to out_args[0]
        Tue May  3 17:26:35 2022 [16]: out_args[2] = submit=allow compacted to out_args[1]
        Tue May  3 17:26:35 2022 [16]: out_args[3] = manage=reject compacted to out_args[2]
        Tue May  3 17:26:35 2022 [16]: 3 output args
        Tue May  3 17:26:35 2022 [16]: authorization query for 'customer' python_tacacs from 172.17.0.1 accepted
        ```

        * Authorization requests are more involved as they have several steps related to determining what key/value pairs are being requested, finding the relevant values for the user, and then sending them back as arguments.  In the above output for `customer`, you can see that only `submit` is set to `allow`.

1. Continue to monitor the log and accounting files as you perform activities in the application. 
1. Now submit a new service request using the application.  

    ![](readme-images/home-customer-new-service-request-01.png)

1. After you submit it, you should now see it listed in the table as "submitted".

    ![](readme-images/home-customer-new-service-request-02.png)

1. A new accounting message is recorded to note this newly requested service. 

    ```
    May  3 17:47:20 172.17.0.1      customer        python_tacacs   dev-network-request     start   service=submit-request message=New Service Request from [customer] for VLAN Name [ReadmeDemo_1] Description [A new network for the readme doc. ]
    ```

### Investigating Service Requests as an Operator 
Now that we've seen how customers can submit requests, log out and back in as `operator` to see what the `read` right adds to users.  

1. Some things to make note of on what the operator sees at logging in. 

    ![](readme-images/home-operator-01.png)

    * The `operator` does **not** see the submission by the `customer` on their home page. 
    * The `operator` has a new **Manage Services** link in the nav bar.  

1. Go ahead and click on this new link for **Manage Services**. 

    ![](readme-images/manage-operator-01.png)

1. Operators have the `read` permission, and this allows them to see all submitted requests.  Including those that came from the sample data when starting the application.  But `operators` lack the `manage` permission, so they can't actually do anything to a request.  
1. Click on the link for **Hello operator** in the nav bar to view the users profile.  

    ![](readme-images/profile-operator-01.png)

    > Note: The `customer` can access their profile as well to view their rights. 

1. On this page  you can clearly see which rights the logged in user has.  This aligns to what is provided during authorization as part of the login process.  
1. If you would like, you can submit a new request as the `operator` as they also have `submit` rights. 

### Managing Requests as the `sysadmin`
Our final user and role is the `sysadmin`.  Go ahead and log out, and back in as `sysadmin`. 

1. Even admins won't see other users requests on the home page. 

    ![](readme-images/home-sysadmin-01.png)

1. Navigate to the **Manage Services** page as `sysadmin`. 

    ![](readme-images/manage-sysadmin-01.png)

1. Now the **Approve** and **Deny** buttons as well as the ability to set/change VLAN Ids is enabled.  This is because `sysadmin` has `manage` rights.  
1. Enter a VLAN Id number, such as `101` for the new request you made and either **Approve** or **Deny** it. The page will update with your changes. 

    ![](readme-images/manage-sysadmin-02.png)

    > Note: If you **Denied** the request, you'll be able to update the VLAN Id.  **Approved** requests can't have the VLAN Id changed.  

1. In the accounting log, you'll see the update action message. 

    ```
    May  3 18:07:57 172.17.0.1      sysadmin        python_tacacs   dev-network-request     start   service=request-management     message=User [sysadmin] taking Service management Action [approved] for UUID [317986707405553680858795039836803158996] VLAN ID [101]
    ```

### Verifying Updated Request as `customer`
Now that request has been approved, if you log back in as the `customer` you should see the service updated.  

![](readme-images/home-customer-02.png)