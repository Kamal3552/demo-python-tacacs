# Network Service Request Application a Demonstrating Python & TACACS
Much of what a network engineer works with as part of their daily work is authenticated using TACACS to some centralized authentication server.  That authentication server may integrate with other systems like Active Directory or LDAP, or a one time token system.  As we begin to build applications or even just scripts as part of NetDevOps or network automation work, we will need to figure out how to ensure proper AAA and RBAC into these systems.  

This project is a sample Python Flask web application for requesting network services, specifically new VLANs, that uses TACACS to authenticate, authorize, and account all activities.

> The functionality of the application to request a new VLAN and then manage those requests is provided simply as a framework for the AAA and TACACS examples.  This application is not presented as a realistic network service management application. 

- [Presentations, Labs, etc](#presentations--labs--etc)
- [Badges](#badges)
- [Quick Start Guide to Run the Application](#quick-start-guide-to-run-the-application)
  * [Requirements](#requirements)
  * [Setting up TACACS Server](#setting-up-tacacs-server)
  * [Setting up Python venv and Running the Application](#setting-up-python-venv-and-running-the-application)
  * [User Accounts](#user-accounts)
- [Support](#support)
- [Roadmap](#roadmap)
- [Known Issues](#known-issues)
- [Contributing](#contributing)
- [Authors and acknowledgment](#authors-and-acknowledgment)
- [License](#license)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Presentations, Labs, etc 
This project was built initially to support conference presentations on the topics of Python + TACACS integration and Introduction to GitLab CICD.  I'll add links to these presentations and details when they are available. 

* [Demonstration of CICD Pipeline Deploying Updates to Application](https://youtu.be/gjUF9XoZNXI)



## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Quick Start Guide to Run the Application 
Here is a quick guide on starting up the application.  For a more detailed walkthrough, see [detailed-guide.md](detailed-guide.md)

### Requirements
You'll need to following installed to run the demo.

* Docker: Used to run the [`tac_plus`](tac_plus/) server the application authenticates to
* Python 3.9: The application itself was written in Python 3.9.4.  New versions of Python should be fine.  Older versions may have issues

### Setting up TACACS Server 

1. Navigate to the `tac_plus/` directory to build and start the server.
1. Build the image using the Makefile

    ```
    make build
    ```

1. Start an instance of the `tac_plus` container in the background with the Makefile. 

    ```
    make run
    ```

### Setting up Python venv and Running the Application 

1. Create and source a new Python 3.9 virtual environment. 

    ```
    python3.9 -m venv venv
    source venv/bin/activate
    ```

1. Install the application using `pip`

    ```
    pip install .
    ```

1. Read the ENVs into your terminal. 

    ```
    source .envrc.dev
    ```

1. Start the Flask application

    ```
    flask run
    ```

1. You can now navigate to the address listed in the output (ie `http://127.0.0.1:5000`). *Details on how to use the application are in the next section.*

    ![App Home Unauthenticated User](readme-images/home-unauth.png)

### User Accounts 

The following are the users configured on the TACACS Server and the group they are a member of. 

|  Username  |  Password  |  TACACS Group |
| ---------  |  --------  |  ------------ |
| `sysadmin` | `password` |  `admin`      |
| `operator` | `password` |  `operator`   |
| `customer` | `password` |  `submitter`  |

Use these accounts when exploring the application. 

## Support
This demonstration application is provided as is without any support.  

## Roadmap
A few things I'd like to add to this project include: 

* Build Makefile targets for installing/starting the application
* Complete the tests written for the application 
    * Log in / log out processing 
    * Service submission and management 
* Create a Dockerfile for the `network_request` application itself 
* Continue work on the CICD file for fully testing and deploying this demo application
* More details on how the `tac_plus` server is configured and used 
* Details and examples for setting up Cisco ISE for providing AAA to the demo application 
* Improve layout and look/feel of the application 

## Known Issues 

* The `tac_plus` server has a bug that allows extra characters at the end of a password.  So a password of `password` configured would allow and accept `password12345`.  This is a bug in the `tac_plus` server and not the application, but I'm interested in seeing if I can resolve this. 
* 

## Contributing
This project is primarily used for some labs and presentations I give on the topic.  As such I'm not really looking for contributions at this time.  

## Authors and acknowledgment
This project makes use of the Python library **tacacs_plus** available on [pypi](https://pypi.org/project/tacacs_plus/) and [GitHub](https://github.com/ansible/tacacs_plus). This library handles much of the low level work building, sending, and receiving messages from a TACACS server.  

For details on TACACS communications and protocol details, [informational RFC 8907](https://www.rfc-editor.org/rfc/rfc8907.html) is an excellent resource.  I learned a great deal about how TACACS worked from this document even after having successfully used TACACS for many years as a network engineer. 

While not suitable for use as a production TACACS server, the [Linux TACACS daemon `tac_plus`](https://shrubbery.net/tac_plus/) that was built on the foundation of Cisco's TACACS+ developers kit is a great resource for developing TACACS related solutions.  

## License
Licensed under MIT License
