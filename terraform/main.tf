terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.16.0"
    }
  }
}

provider "aws" {
  region = local.region
}

locals {
  name   = "demo-python-tacacs-${var.ENVIRONMENT}"
  region = "us-east-2"

  tags = {
    Owner       = "hapresto"
    Environment = var.ENVIRONMENT
    Demo        = "python-tacacs"
  }
}

################################################################################
# Create the Docker Host for Running the application 
# in AWS including required network and security objects.
################################################################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name = local.name
  cidr = "10.99.0.0/18"

  azs            = ["${local.region}a"]
  public_subnets = ["10.99.0.0/24"]
  #   private_subnets  = ["10.99.3.0/24"]

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = local.tags
}

# Leverage the AWS ECS optimized ami 
# https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
data "aws_ami" "amazon_linux_ecs" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-*-amazon-ecs-optimized"]
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = local.name
  description = "Access rules for the network-request app and host"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "ssh-tcp"]
  egress_rules        = ["all-all"]

  tags = local.tags
}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "${local.name}-docker-host"

  ami                         = data.aws_ami.amazon_linux_ecs.id
  instance_type               = "t2.micro"
  key_name                    = "demo-tacacs"
  availability_zone           = element(module.vpc.azs, 0)
  monitoring                  = true
  vpc_security_group_ids      = [module.security_group.security_group_id]
  subnet_id                   = element(module.vpc.public_subnets, 0)
  associate_public_ip_address = true

  tags = local.tags
}

################################################################################
# Manage the Container Deployment for the Application on 
# on the deployed EC2 Docker Host
################################################################################

# Read in keys for host
resource "null_resource" "docker-keys" {
  provisioner "local-exec" {
    command = "ssh-keyscan ${module.ec2_instance.public_ip}"
  }
}

# Docker Image management 
provider "docker" {
  host     = "ssh://ec2-user@${module.ec2_instance.public_ip}:22"
  ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null", "-i", var.PROD_SSH_KEY]
}

# Check hash for image from registry 
data "docker_registry_image" "tacplus_server" {
  name = "${var.docker_registry}/${var.tacplus_image_path}:${var.tacplus_image_tag}"
}

data "docker_registry_image" "network_request_app" {
  name = "${var.docker_registry}/${var.network_service_image_path}:${var.network_service_image_tag}"
}

# Pull image from registry 
resource "docker_image" "tacplus_server" {
  name          = data.docker_registry_image.tacplus_server.name
  pull_triggers = [data.docker_registry_image.tacplus_server.sha256_digest]
}

resource "docker_image" "network_request_app" {
  name          = data.docker_registry_image.network_request_app.name
  pull_triggers = [data.docker_registry_image.network_request_app.sha256_digest]
}

# Create docker network for application
resource "docker_network" "network_request" {
  name       = "network_request"
  driver     = "bridge"
  attachable = true

}

# Start tac_plus server
resource "docker_container" "tac_plus" {
  depends_on = [docker_image.tacplus_server]
  name       = "tac_plus"
  image      = docker_image.tacplus_server.repo_digest
  tty        = true
  stdin_open = true
  restart    = "unless-stopped"
  env        = []
  networks_advanced {
    name = docker_network.network_request.name
    aliases = [
      "tacacs_server",
      "tacplus",
    ]
  }
}

# Start network_request app
resource "docker_container" "network_request" {
  depends_on = [docker_container.tac_plus]
  name       = "network_request"
  image      = docker_image.network_request_app.repo_digest
  tty        = true
  stdin_open = true
  restart    = "unless-stopped"
  env = [
    "TACACS_HOST=${var.tacacs_host}",
    "TACACS_SECRET=${var.tacacs_secret}",
    "LOAD_SAMPLE_DATA=${var.load_sample_data}",
  ]
  #   links = [
  #       "tac_plus:tacacs_server",
  #   ]
  ports {
    internal = 5000
    external = 80
  }
  networks_advanced {
    name = docker_network.network_request.name
  }
}
