variable "docker_registry" { default = "registry.gitlab.com" }
variable "tacplus_image_path" { default = "hpreston/demo-python-tacacs/tac_plus" }
variable "tacplus_image_tag" { default = "main" }

variable "network_service_image_path" { default = "hpreston/demo-python-tacacs/network-request" }

# TODO: This needs to be able to be input from CICD pipeline during deployment
variable "network_service_image_tag" { default = "main" }

# network_request application configuration settings 
variable "tacacs_host" { default = "tacacs_server" }
variable "tacacs_secret" { default = "labtacacskey" }
variable "load_sample_data" { default = "yes" }

variable "PROD_SSH_KEY" { default = "~/.ssh/aws-demo-tacacs.pem" }

variable "ENVIRONMENT" { default = "none" }