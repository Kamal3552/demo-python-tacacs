
SHELL := /bin/bash
DEPLOY_TYPE ?= dev

test: 
	source .envrc.dev && pytest

run: 
	source venv/bin/activate && source .envrc.dev && flask run

# Docker Targets for Building and Testing in Development
build-docker: 
	docker build \
	  -t network_request:latest \
	  .

clean: stop delete

run-docker: 
	docker run -it -d \
		--name network_request \
		-p 80:5000 \
		--link tac_plus:tacacs_server \
		-e TACACS_SECRET=labtacacskey \
		-e LOAD_SAMPLE_DATA=yes \
		network_request:latest 

run-docker-shell: 
	docker run -it --rm \
		--name network_request \
		-p 80:5000 \
		--link tac_plus:tacacs_server \
		-e TACACS_SECRET=labtacacskey \
		-e LOAD_SAMPLE_DATA=yes \
		network_request:latest \
		/bin/bash

stop-docker: 
	docker stop network_request 

start-docker: 
	docker start network_request

delete-docker: 
	docker rm network_request 

attach-docker: 
	docker attach network_request




